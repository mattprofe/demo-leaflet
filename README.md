DEMO-Leaflet
=========
Ejemplo de uso de la libreria Leaflet

Se utiliza para su desarollo y pruebas:

- HTML.
- CSS.
- Javascript.
- PHP

Temas que se aplican
====================

- Manejo correcto de etiquetas HTML5.
- Uso de reglas CSS3.
- Lógica de programación en JS/PHP y buenas prácticas.
- Uso de libreria Leaflet.

Uso
===

- Colocar el proyecto dentro del servidor y descomprimirlo.
- Acceder con un navegador a la carpeta demo-leaflet-master.
- Para ver el mapa generado por Leaflet abrir map_tracker.php.
- Para ver la latitud y longitud de un cliente (ip pública) abrir ip_tracker.php.

Contenido
=========
- ip_tracker.php: Obtiene la ip pública del cliente y recupera más información de ella utilizando la API de http://ipwho.is/.
- map_tracker.php: Genera un mapa con marcadores y popups utilizando la libreria Leaflet, las ubicaciones de los marcadores se obtiene del archivo data.json.
- data.json: contiene información de clientes que accedieron a una web, cada fila tiene el formato siguiente {"ip":"104.28.59.3","cantidad":"4","latitud":"-27.4692131","longitud":"-58.8306349"}.

Autor
=====
- Matias Baez
- @matt_profe
- mbcorp.matias@gmail.com
- https://mattprofe.com.ar